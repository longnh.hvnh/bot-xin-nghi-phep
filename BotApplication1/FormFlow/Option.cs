﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;

namespace BotApplication1.FormFlow
{
    [Serializable]
    public class Option
    {
        [Prompt("Hãy lựa chọn chức năng {||}")]
        public ChucNang? ChucNang { set; get; }

        [Prompt("Chọn loại nghỉ {||}")]
        public LoaiNghi? LoaiNghi { set; get; }

        [Prompt("Lý do xin nghỉ là gì ?")]
        public string LyDoNghi { set; get; }

        [Prompt("Nhập thời gian nghỉ (dd/mm/yyyy)")]
        public DateTime ThoiGianNghi { set; get; }


        public static int Add(int a, int b)
        {
            return a + b;
        }

        public static IForm<Option> BuildForm()
        {
            OnCompletionAsyncDelegate<Option> completeForm = async (context, state) =>
            {
                try
                {
                    var chucnang = state.ChucNang;
                    var loainghi = state.LoaiNghi;
                    var lydonghi = state.LyDoNghi;
                    var thoigiannghi = state.ThoiGianNghi;
                    if (state.ChucNang == FormFlow.ChucNang.XinNghi)
                    {
                        // Goi ham o day
                        //Add(1, 2);
                        await context.PostAsync(Add(1, 2).ToString());
                        //await context.PostAsync(chucnang.ToString() + loainghi + lydonghi + thoigiannghi);
                    }
                    else
                    {
                        await context.PostAsync("Chua dc ho tro");
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                await context.PostAsync("Yêu cầu của bạn đã được gửi đi.");

            };

            return new FormBuilder<Option>()
                .Message("Một ngày đẹp trời, bạn có muốn nghỉ ko >> ?")
                .Field(nameof(ChucNang))
                .Field(nameof(LoaiNghi))
                .Field(nameof(LyDoNghi))
                .Field(nameof(ThoiGianNghi))
                .Confirm("Bạn xác nhận thực hiện ( Y / N ) ?")
                .OnCompletion(completeForm)
                .Build();
        }
    };

    [Serializable]
    public enum ChucNang
    {
        XinNghi, TraCuuThoiGianNghi
    };

    [Serializable]
    public enum LoaiNghi
    {
        NghiSang, NghiChieu, NghiCaNgay, RaNgoaiKhoangThoiGian
    };
}